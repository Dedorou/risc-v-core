# RISC-V core

Multi-cycle RISC-V core which supports execution of instructions of the [RV32I](https://five-embeddev.com/riscv-isa-manual/latest/rv32.html) specification and memory-mapped I/O 

Top module is [hdl/RISC_V_core.v][def]



[def]: https://gitlab.com/Dedorou/risc-v-core/-/blob/main/hdl/RISC_V_core.v?ref_type=heads
`include "parameters.vh"

module memory_block (

input clk,
input rst,

//адресные мультиплексоры
input [`address_width : 2] CU_A1_out,	//[`address_width - 1 : 0]
input [`address_width - 1 : 2] CU_A2_out,	//[`address_width - 1 : 0]
input [`word_width - 1 : 0] ALU_out,
input [`mem_A1_mux_control - 1 : 0] A1_mux_control,
input A2_mux_control,

//мультиплексоры записи
input W1_mux_control,
input W2_mux_control,

//управление записью
input [`WE_width - 1 : 0] WE1,
input [`WE_width - 1 : 0] WE2,

//счетчики команд
input pc_WE,
input old_pc_WE,

input Port_WE,
input [15 : 0] SW_in,

output [`word_width - 1 : 0] R1,
output [`word_width - 1 : 0] R2,

output [`word_width - 1 : 0] pc_out,
output [`word_width - 1 : 0] old_pc_out,

output [15 : 0] LEDs_out,
output [`word_width - 1 : 0] port_out,
output Port_Reg,
input Out_Port_WE,

input btnC_in,
input btnU_in,
input btnL_in,
input btnR_in,
input btnD_in,

output [6 : 0] Sev_seg_out,
output [7 : 0] An_out
);

wire [`address_width : 2] A1_mux_out;	//[`address_width - 1 : 0]
wire [`address_width - 1 : 2] A2_mux_out;	//[`address_width - 1 : 0]
wire [`word_width - 1 : 0] W1_mux_out;
wire [`word_width - 1 : 0] W2_mux_out;


wire [3 : 0] WE1_p;
assign WE1_p[0] = WE1[0] && (~A1_mux_out[`address_width]);
assign WE1_p[1] = WE1[1] && (~A1_mux_out[`address_width]);
assign WE1_p[2] = WE1[2] && (~A1_mux_out[`address_width]);
assign WE1_p[3] = WE1[3] && (~A1_mux_out[`address_width]);
//assign WE1_p[0] =  Port_WE && WE1[0] && (~A1_mux_out[`address_width]);	
//assign WE1_p[1] =  Port_WE && WE1[1] && (~A1_mux_out[`address_width]);	
//assign WE1_p[2] =  Port_WE && WE1[2] && (~A1_mux_out[`address_width]);	
//assign WE1_p[3] =  Port_WE && WE1[3] && (~A1_mux_out[`address_width]);


memory #(`instr_block_size, `data_block_size) memory (
	.clk (clk),
	.A1 (A1_mux_out[`address_width - 1 : 2]),
	.A2 (A2_mux_out),
	.W1 (W1_mux_out),
	.W2 (W2_mux_out),
	.WE1 (WE1_p),
	.WE2 (WE2),
	.R1 (R1),
	.R2 (R2));

pc pc (
	.clk (clk),
	.rst (rst),
	.pc_WE (pc_WE),
	.pc_in (ALU_out),
	.pc_out(pc_out));
	
reg_32 old_pc_reg (
	.clk (clk),
	.rst (rst),
	.WE (old_pc_WE),
	.D (pc_out),
	.Q (old_pc_out));

mux_3_addr_port A1_mux (
	.a (CU_A1_out),
	.b (pc_out[`address_width : 2]),	//[`address_width - 1 : 0]
	.c (ALU_out[`address_width : 2]),	//[`address_width - 1 : 0]
	.sel (A1_mux_control),
	.out (A1_mux_out));

mux_2_addr A2_mux (
	.a (CU_A2_out),
	.b (ALU_out[`address_width - 1 : 2]),	//[`address_width - 1 : 0]
	.sel (A2_mux_control),
	.out (A2_mux_out));

mux_2 W1_mux (
	.a (ALU_out),
	.b (R2),
	.sel (W1_mux_control),
	.out (W1_mux_out));

mux_2 W2_mux (
	.a (ALU_out),
	.b (pc_out),
	.sel (W2_mux_control),
	.out (W2_mux_out));
	
			
I_O_module # (
	.LEDs_Addr (`LEDs_Addr),
	.SW_Addr (`SW_Addr),
	.digit_1_addr (`digit_1_addr),
	.digit_2_addr (`digit_2_addr),
	.digit_3_addr (`digit_3_addr),
	.digit_4_addr (`digit_4_addr),
	.digit_5_addr (`digit_5_addr),
	.digit_6_addr (`digit_6_addr),
	.digit_7_addr (`digit_7_addr),
	.digit_8_addr (`digit_8_addr),
	.btnC_addr (`btnC_addr),
	.btnU_addr (`btnU_addr),
	.btnL_addr (`btnL_addr),
	.btnR_addr (`btnR_addr),
	.btnD_addr (`btnD_addr))
	I_O_module(
	.clk (clk),
	.rst (rst),
	.data_in (W1_mux_out[`max_port_width - 1 : 0]),
	.addr (A1_mux_out),
	.Port_WE (Port_WE),
	.LEDs_out (LEDs_out),
	.Out_Port_WE (Out_Port_WE),
	.Port_Reg (Port_Reg),
	.port_out (port_out),
	.Sev_seg_out (Sev_seg_out),
	.An_out (An_out),
	.SW_in (SW_in),
	.btnC_in (btnC_in),
	.btnU_in (btnU_in),
	.btnL_in (btnL_in),
	.btnR_in (btnR_in),
	.btnD_in (btnD_in));

endmodule
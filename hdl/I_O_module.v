`include "parameters.vh"

module I_O_module #(
parameter LEDs_Addr = `LEDs_Addr,
parameter SW_Addr = `SW_Addr,
parameter digit_1_addr = `digit_1_addr,
parameter digit_2_addr = `digit_2_addr,
parameter digit_3_addr = `digit_3_addr,
parameter digit_4_addr = `digit_4_addr,
parameter digit_5_addr = `digit_5_addr,
parameter digit_6_addr = `digit_6_addr,
parameter digit_7_addr = `digit_7_addr,
parameter digit_8_addr = `digit_8_addr,
parameter btnC_addr = `btnC_addr,
parameter btnU_addr = `btnU_addr,
parameter btnL_addr = `btnL_addr,
parameter btnR_addr = `btnR_addr,
parameter btnD_addr = `btnD_addr
)(
input clk, 
input rst,
input [`max_port_width - 1 : 0]	data_in,
input [`address_width : 2] addr,
input Port_WE,
input Out_Port_WE,
input [15 : 0] SW_in,
input btnC_in,
input btnU_in,
input btnL_in,
input btnR_in,
input btnD_in,
output Port_Reg,
output [15 : 0] LEDs_out,
output [6 : 0] Sev_seg_out,
output [7 : 0] An_out,
output [`word_width - 1 : 0] port_out);

//LEDs

wire LEDs_WE;
reg LEDs_eq;

always @(*) begin 
	if (addr == LEDs_Addr[`address_width : 2]) begin 
		LEDs_eq <= 1;
	end else begin 	
		LEDs_eq <= 0;
	end
end

assign LEDs_WE = LEDs_eq && Port_WE;

param_reg #(16) LEDs (clk, 1'b0, LEDs_WE, data_in, LEDs_out);

//swiches

wire SW_WE;
reg SW_eq;
wire [15 : 0] SW_out;

always @(*) begin 
	if (addr == SW_Addr[`address_width : 2]) begin 
		SW_eq <= 1;
	end else begin 	
		SW_eq <= 0;
	end
end

assign SW_WE = SW_eq && Out_Port_WE;

param_reg #(16) SW (clk, 1'b0, SW_WE, SW_in, SW_out);


//7_seg

wire Sev_seg_WE;
reg Sev_seg_eq;
reg [7 : 0] an;


always @(*) begin 

case (addr)
		digit_1_addr[`address_width : 2] : begin 
				an <= 8'b01111111;
				Sev_seg_eq <= 1;
			end
		digit_2_addr[`address_width : 2] : begin 
				an <= 8'b10111111;
				Sev_seg_eq <= 1;
			end
		digit_3_addr[`address_width : 2] : begin 
				an <= 8'b11011111;
				Sev_seg_eq <= 1;
			end
		digit_4_addr[`address_width : 2] : begin 
				an <= 8'b11101111;
				Sev_seg_eq <= 1;
			end
		digit_5_addr[`address_width : 2] : begin 
				an <= 8'b11110111;
				Sev_seg_eq <= 1;
			end
		digit_6_addr[`address_width : 2] : begin 
				an <= 8'b11111011;
				Sev_seg_eq <= 1;
			end
		digit_7_addr[`address_width : 2] : begin
				an <= 8'b11111101;
				Sev_seg_eq <= 1;
			end
		digit_8_addr[`address_width : 2] : begin 
				an <= 8'b11111110;
				Sev_seg_eq <= 1;
			end
		default : begin 
				an <= 8'b11111111;
				Sev_seg_eq <= 0;
			end
endcase 

end

assign Sev_seg_WE = Sev_seg_eq && Port_WE;

an_reg #(8) ann (clk, 1'b0, Sev_seg_WE, an, An_out);
an_reg #(7) sev_seg (clk, 1'b0, Sev_seg_WE, data_in[6 : 0], Sev_seg_out);

////

//buttons 
//btnC
wire btnC_WE;
reg btnC_eq;
wire btnC_out;
always @(*) begin 
	if (addr == btnC_addr[`address_width : 2]) begin 
		btnC_eq <= 1;
	end else begin 	
		btnC_eq <= 0;
	end
end
assign btnC_WE = btnC_eq && Out_Port_WE;
param_reg #(1) btnC (clk, rst, btnC_WE, btnC_in, btnC_out);

//btnU
wire btnU_WE;
reg btnU_eq;
wire btnU_out;
always @(*) begin 
	if (addr == btnU_addr[`address_width : 2]) begin 
		btnU_eq <= 1;
	end else begin 	
		btnU_eq <= 0;
	end
end
assign btnU_WE = btnU_eq && Out_Port_WE;
param_reg #(1) btnU (clk, rst, btnU_WE, btnU_in, btnU_out);

//btnL
wire btnL_WE;
reg btnL_eq;
wire btnL_out;
always @(*) begin 
	if (addr == btnL_addr[`address_width : 2]) begin 
		btnL_eq <= 1;
	end else begin 	
		btnL_eq <= 0;
	end
end
assign btnL_WE = btnL_eq && Out_Port_WE;
param_reg #(1) btnL (clk, rst, btnL_WE, btnL_in, btnL_out);

//btnR
wire btnR_WE;
reg btnR_eq;
wire btnR_out;
always @(*) begin 
	if (addr == btnR_addr[`address_width : 2]) begin 
		btnR_eq <= 1;
	end else begin 	
		btnR_eq <= 0;
	end
end
assign btnR_WE = btnR_eq && Out_Port_WE;
param_reg #(1) btnR (clk, rst, btnR_WE, btnR_in, btnR_out);

//btnD
wire btnD_WE;
reg btnD_eq;
wire btnD_out;
always @(*) begin 
	if (addr == btnD_addr[`address_width : 2]) begin 
		btnD_eq <= 1;
	end else begin 	
		btnD_eq <= 0;
	end
end
assign btnD_WE = btnD_eq && Out_Port_WE;
param_reg #(1) btnD (clk, rst, btnD_WE, btnD_in, btnD_out);

//assign Port_Reg = SW_WE;

assign Port_Reg = SW_WE || btnC_WE || btnU_WE || btnL_WE || btnR_WE || btnD_WE;

wire [`address_width : 2] addr_out;
wire [2 : 0] addr_mux_contr;

param_reg #(`address_width - 1) addr_reg (clk, rst, Port_Reg, addr, addr_out);

addr_dec addr_dec (addr_out, addr_mux_contr);

addr_dmux addr_dmux (SW_out, btnC_out, btnU_out, btnL_out, btnR_out, btnD_out, addr_mux_contr, port_out);

endmodule


module addr_dec #(
parameter SW_Addr = `SW_Addr,
parameter btnC_addr = `btnC_addr,
parameter btnU_addr = `btnU_addr,
parameter btnL_addr = `btnL_addr,
parameter btnR_addr = `btnR_addr,
parameter btnD_addr = `btnD_addr
)(
input [`address_width : 2] addr,
output reg [2 : 0] add_mux_contr);

always @(*) begin 
	case (addr)
		SW_Addr[`address_width : 2] : add_mux_contr <= 3'b000;
		btnC_addr[`address_width : 2] : add_mux_contr <= 3'b001;
		btnU_addr[`address_width : 2] : add_mux_contr <= 3'b010;
		btnL_addr[`address_width : 2] : add_mux_contr <= 3'b011;
		btnR_addr[`address_width : 2] : add_mux_contr <= 3'b100;
		btnD_addr[`address_width : 2] : add_mux_contr <= 3'b101;
		default : add_mux_contr <= 3'b000;
	endcase 
end

endmodule 

module addr_dmux (
input [15 : 0] SW,
input btnC,
input btnU,
input btnL,
input btnR,
input btnD,
input [2 : 0] sel,
output reg [`word_width - 1 : 0] data_out);

always @(*) begin 
	case (sel)
		3'b000 : data_out <= {17'b0, SW};
		3'b001 : data_out <= {31'b0, btnC};
		3'b010 : data_out <= {31'b0, btnU};
		3'b011 : data_out <= {31'b0, btnL};
		3'b100 : data_out <= {31'b0, btnR};
		3'b101 : data_out <= {31'b0, btnD};
		default : data_out <= {16'b0, SW};
	endcase 
end

endmodule 


// ram_WE = (~port_WE & WE & A) || (~port_WE & WE & A)
// port 
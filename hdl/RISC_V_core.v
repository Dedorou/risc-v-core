`include "parameters.vh"

module RISC_V_core (
input clk, 
input rst,
input [15 : 0] sw,
input btnC,
input btnU,
input btnL,
input btnR,
input btnD,
output [7 : 0] an,
output [6 : 0] seg,
output [15 : 0] led);

wire [`address_width : 2] mem_A1;	//[`address_width - 1 : 0]
wire [`address_width - 1 : 2] mem_A2;	//[`address_width - 1 : 0]
wire [`word_width - 1 : 0] mem_R1;
wire [`word_width - 1 : 0] mem_R2;
wire [`WE_width - 1 : 0] mem_WE1;
wire [`WE_width - 1 : 0] mem_WE2;
wire [`mem_A1_mux_control - 1 : 0] mem_A1_mux_control;
wire mem_A2_mux_control;
wire mem_W1_mux_control;
wire mem_W2_mux_control;

wire old_pc_reg_WE;
wire pc_WE;
wire [`word_width - 1 : 0] old_pc_reg_out;
wire [`word_width - 1 : 0] pc_out;

wire branch;
wire [`ALU_control_width - 1 : 0] ALU_control;
wire [`ALU_mux_1_control_width - 1 : 0] ALU_mux_1_control;
wire [`ALU_mux_2_control_width - 1 : 0] ALU_mux_2_control;

wire [`imm_control_width - 1 : 0] imm_control;
wire [`word_width - 1 : `opcode_width] imm;

wire [`word_width - 1 : 0] ALU_out;

wire Port_WE;

wire Out_Port_WE;
wire Port_Reg;

wire [15 : 0] LEDs_out;

assign led = LEDs_out;
wire [`word_width - 1 : 0] port_out;

//wire rst1;
//
//debounce_better_version debounce_better_version (rst, clk, rst1);


control_decode_block control_decode_block(
	.clk (clk),
	.rst (rst),
	.instruction (mem_R1),
	//ALU
	.branch (branch),
	.ALU_control (ALU_control),
	.ALU_mux_1 (ALU_mux_1_control),
	.ALU_mux_2 (ALU_mux_2_control),
	//imm extention
	.imm_control (imm_control),
	.imm (imm),
	//memory
	.mem_A1 (mem_A1),
	.mem_A2 (mem_A2),
	.mem_WE1 (mem_WE1),
	.mem_WE2 (mem_WE2),
	.mem_A1_mux (mem_A1_mux_control),
	.mem_A2_mux (mem_A2_mux_control),
	.mem_W1_mux (mem_W1_mux_control),
	.mem_W2_mux (mem_W2_mux_control),
	//pc
	.old_pc_reg_WE (old_pc_reg_WE),
	.pc_WE (pc_WE),
	.Port_WE (Port_WE),
	.Out_Port_WE (Out_Port_WE),
	.Port_Reg (Port_Reg));
	
memory_block memory_block (
	.clk (clk),
	.rst (rst),
	
	.CU_A1_out (mem_A1),
	.CU_A2_out (mem_A2),
	.ALU_out (ALU_out),
	.A1_mux_control (mem_A1_mux_control),
	.A2_mux_control (mem_A2_mux_control),
	
	.W1_mux_control (mem_W1_mux_control),
	.W2_mux_control (mem_W2_mux_control),
	
	.WE1 (mem_WE1),
	.WE2 (mem_WE2),
	
	.pc_WE (pc_WE),
	.old_pc_WE (old_pc_reg_WE),
	.Port_WE (Port_WE),
	
	.R1 (mem_R1),
	.R2 (mem_R2),
	
	.pc_out (pc_out),
	.old_pc_out (old_pc_reg_out),
	.LEDs_out (LEDs_out),
	.SW_in (sw),
	
	.Sev_seg_out (seg),
	.An_out (an),
	
	.btnC_in (btnC),
	.btnU_in (btnU),
	.btnL_in (btnL),
	.btnR_in (btnR),
	.btnD_in (btnD),

	.Out_Port_WE (Out_Port_WE),
	.Port_Reg (Port_Reg),
	.port_out (port_out));

ALU_block ALU_block (
	.ALU_control (ALU_control),
	.imm_control (imm_control),
	.imm_in (imm),
	.ALU_mux_1_control (ALU_mux_1_control),
	.ALU_mux_2_control (ALU_mux_2_control),
	.mem_R1_ALU_in (mem_R1),
	.pc_ALU_in (pc_out),
	.old_pc_ALU_in (old_pc_reg_out),
	.mem_R2_ALU_in (mem_R2),
	.branch (branch),
	.ALU_out (ALU_out),
	.port_out (port_out));

endmodule

module debounce_better_version(input pb_1,clk,output pb_out);
wire slow_clk_en;
wire Q1,Q2,Q2_bar,Q0;
clock_enable u1(clk,slow_clk_en);
my_dff_en d0(clk,slow_clk_en,pb_1,Q0);

my_dff_en d1(clk,slow_clk_en,Q0,Q1);
my_dff_en d2(clk,slow_clk_en,Q1,Q2);
assign Q2_bar = ~Q2;
assign pb_out = Q1 & Q2_bar;
endmodule
// Slow clock enable for debouncing button 
module clock_enable(input Clk_100M,output slow_clk_en);
    reg [26:0]counter=0;
    always @(posedge Clk_100M)
    begin
       counter <= (counter>=249999)?0:counter+1;
    end
    assign slow_clk_en = (counter == 249999)?1'b1:1'b0;
endmodule
// D-flip-flop with clock enable signal for debouncing module 
module my_dff_en(input DFF_CLOCK, clock_enable,D, output reg Q=0);
    always @ (posedge DFF_CLOCK) begin
  if(clock_enable==1) 
           Q <= D;
    end
endmodule 